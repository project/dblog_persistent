<?php

namespace Drupal\dblog_persistent;

/**
 * Interface DbLogPersistentLoaderInterface.
 */
interface DbLogPersistentStorageInterface {

  /**
   * List all types currently stored.
   *
   * @return string[]
   *   List of types.
   */
  public function getTypes(): array;

  /**
   * Count the messages of a channel.
   *
   * @param string $channel
   *   Channel ID.
   *
   * @return int
   *   Number of messages stored in the channel.
   */
  public function countChannel(string $channel): int;

  /**
   * Delete all messages in a channel.
   *
   * @param string $channel
   *   Channel ID.
   *
   * @return int
   *   Returns the number of entries deleted.
   */
  public function clearChannel(string $channel): int;

  /**
   * Write log message.
   *
   * @param string $channel
   *   Channel ID.
   * @param array $fields
   *   Values of the record to be inserted.
   */
  public function writeLog(string $channel, array $fields);

  /**
   * Get messages from a specific channel.
   *
   * @param string $channel
   *   The channel to select.
   * @param int|null $count
   *   The page size.
   * @param array|null $header
   *   An optional table array to use for sorting.
   *
   * @return iterable
   *   List of messages.
   */
  public function getChannel(string $channel, int $count = NULL, array $header = NULL): iterable;

  /**
   * Retrieve a single event.
   *
   * @param int $event_id
   *   Event ID.
   *
   * @return object|null
   *   Values of the event record, or NULL if it does not exist.
   */
  public function getEvent(int $event_id): ?object;

}
