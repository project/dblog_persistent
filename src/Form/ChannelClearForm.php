<?php

namespace Drupal\dblog_persistent\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\dblog_persistent\DbLogPersistentStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Remove all log messages stored in a persistent channel.
 */
class ChannelClearForm extends EntityConfirmFormBase {

  /**
   * The dblog_persistent.storage service.
   *
   * @var \Drupal\dblog_persistent\DbLogPersistentStorageInterface
   */
  protected DbLogPersistentStorageInterface $storage;

  /**
   * ChannelClearForm constructor.
   *
   * @param \Drupal\dblog_persistent\DbLogPersistentStorageInterface $storage
   *   The dblog_persistent.storage service.
   */
  public function __construct(DbLogPersistentStorageInterface $storage) {
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static($container->get('dblog_persistent.storage'));
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Clear the persistent log channel %channel?', [
      '%channel' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\Exception\UndefinedLinkTemplateException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getCancelUrl(): Url {
    return $this->entity->toUrl('collection');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\Exception\UndefinedLinkTemplateException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    if ($count = $this->storage->clearChannel($this->entity->id())) {
      $this->messenger()->addStatus($this->t('Deleted %count log messages from channel %channel.', [
        '%count' => $count,
        '%channel' => $this->entity->label(),
      ]));
    }
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
