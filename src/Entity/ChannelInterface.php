<?php

namespace Drupal\dblog_persistent\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Persistent Log Message Type entities.
 */
interface ChannelInterface extends ConfigEntityInterface {

  /**
   * Get all types matched by this filter.
   *
   * @return string[]
   *   List of types.
   */
  public function getTypes(): array;

  /**
   * Get the severities matched by this filter.
   *
   * @return int[]
   *   List of severities.
   */
  public function getLevels(): array;

  /**
   * Get the message substring matched by this filter.
   *
   * @return string
   *   Message substring.
   */
  public function getMessage(): string;

  /**
   * Check a given message against the configured filters.
   *
   * @param int $level
   *   Severity level.
   * @param string $type
   *   Event type.
   * @param string $message
   *   Message content.
   *
   * @return bool
   *   TRUE if the message matches all filters.
   */
  public function matches(int $level, string $type, string $message): bool;

}
