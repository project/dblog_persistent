<?php

namespace Drupal\dblog_persistent\Logger;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\dblog\Logger\DbLog;
use Drupal\dblog_persistent\DbLogPersistentStorageInterface;
use Drupal\dblog_persistent\Entity\ChannelInterface;

/**
 * Persistent logger service.
 */
class DbLogPersistent extends DbLog {

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $manager;

  /**
   * The dblog_persistent.storage service.
   *
   * @var \Drupal\dblog_persistent\DbLogPersistentStorageInterface
   */
  protected DbLogPersistentStorageInterface $storage;

  /**
   * All dblog_persistent_channel entities.
   *
   * @var \Drupal\dblog_persistent\Entity\ChannelInterface[]
   */
  private array $channels;

  /**
   * DbLogPersistent constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The logger.log_message_parser service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $manager
   *   The entity_type.manager service.
   * @param \Drupal\dblog_persistent\DbLogPersistentStorageInterface $storage
   *   The dblog_persistent.storage service.
   */
  public function __construct(Connection $connection,
                              LogMessageParserInterface $parser,
                              EntityTypeManagerInterface $manager,
                              DbLogPersistentStorageInterface $storage) {
    parent::__construct($connection, $parser);
    $this->manager = $manager;
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   *
   * This function must be copied in order to specify a different table.
   *
   * @throws \PDOException
   */
  public function log($level, $message, array $context = []): void {
    // Remove any backtraces since they may contain an unserializable variable.
    unset($context['backtrace']);
    $args = $this->parser->parseMessagePlaceholders($message, $context);
    // Insert variables, but use untranslated string and strip markup.
    $output = \strip_tags(new FormattableMarkup($message, $args));
    foreach ($this->getChannels($level, $output, $context) as $channel) {
      $this->writeLog($channel->id(), $level, $message, $args, $context);
    }
  }

  /**
   * List the channels that a message fits into.
   *
   * @param int $level
   *   The severity level.
   * @param string $message
   *   The message content.
   * @param array $context
   *   The message context variables.
   *
   * @return \Drupal\dblog_persistent\Entity\ChannelInterface[]
   *   List of persistent log channels.
   */
  protected function getChannels(int $level,
                                 string $message,
                                 array $context): array {
    if (!isset($this->channels)) {
      $this->channels = [];
      try {
        $this->channels = $this->manager
          ->getStorage('dblog_persistent_channel')
          ->loadMultiple();
      }
      // Prevent errors during module installation.
      catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      }
    }

    return array_filter(
      $this->channels,
      static function (ChannelInterface $channel) use ($level, $message, $context) {
        return $channel->matches($level, $context['channel'], $message);
      }
    );
  }

  /**
   * Write the log into a specific channel.
   *
   * @param string $channel
   *   The channel ID.
   * @param int $level
   *   The severity level.
   * @param string $message
   *   The message content.
   * @param array $args
   *   The message arguments.
   * @param array $context
   *   The message context variables.
   */
  protected function writeLog(string $channel, int $level, string $message, array $args, array $context): void {
    $this->storage->writeLog($channel, [
      'uid'       => $context['uid'],
      'type'      => mb_substr($context['channel'], 0, 64),
      'message'   => $message,
      'variables' => serialize($args),
      'severity'  => $level,
      'link'      => $context['link'],
      'location'  => $context['request_uri'],
      'referer'   => $context['referer'],
      'hostname'  => mb_substr($context['ip'], 0, 128),
      'timestamp' => $context['timestamp'],
    ]);
  }

}
