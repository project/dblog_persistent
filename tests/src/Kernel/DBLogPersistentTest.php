<?php

namespace Drupal\Tests\dblog_persistent\Kernel;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\dblog_persistent\DbLogPersistentStorageInterface;
use Drupal\dblog_persistent\Entity\Channel;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test basic functionality of the module.
 *
 * @group dblog_persistent
 */
class DBLogPersistentTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dblog',
    'dblog_persistent',
    'system',
    'user',
  ];

  /**
   * The dblog_persistent.storage service.
   *
   * @var \Drupal\dblog_persistent\DbLogPersistentStorageInterface
   */
  private DbLogPersistentStorageInterface $storage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('dblog', 'watchdog');
    $this->installSchema('dblog_persistent', 'dblog_persistent');
    $this->installEntitySchema('user');
    $this->installConfig([
      'dblog',
      'dblog_persistent',
    ]);
    $this->storage = \Drupal::service('dblog_persistent.storage');
  }

  /**
   * Test functionality.
   */
  public function testPersistentLog(): void {
    Channel::create([
      'id' => 'channel1',
      'label' => 'Channel 1',
      'types' => ['type1' => 'type1'],
      'levels' => [RfcLogLevel::INFO => RfcLogLevel::INFO],
      'message' => 'substring',
    ])->save();

    Channel::create([
      'id' => 'channel2',
      'label' => 'Channel 2',
    ])->save();

    $logger1 = \Drupal::logger('type1');
    $logger1->info('1: This message contains substring and should be logged.');
    $logger1->info('2: This message should not be logged.');
    $logger1->warning('3: This message contains substring and should not be logged.');

    $logger2 = \Drupal::logger('type2');
    $logger2->info('4: This message contains substring and should not be logged.');

    $messages = [];
    foreach ($this->storage->getChannel('channel1') as $record) {
      $messages[] = [
        'type'     => $record->type,
        'severity' => $record->severity,
        'message'  => $record->message,
      ];
    }
    $this->assertEquals([
      [
        'type'     => 'type1',
        'severity' => RfcLogLevel::INFO,
        'message'  => '1: This message contains substring and should be logged.',
      ],
    ], $messages);

    $messages = [];
    foreach ($this->storage->getChannel('channel2') as $record) {
      $messages[] = [
        'type'     => $record->type,
        'severity' => $record->severity,
        'message'  => $record->message,
      ];
    }
    $this->assertEquals([
      [
        'type'     => 'type1',
        'severity' => RfcLogLevel::INFO,
        'message'  => '1: This message contains substring and should be logged.',
      ],
      [
        'type'     => 'type1',
        'severity' => RfcLogLevel::INFO,
        'message'  => '2: This message should not be logged.',
      ],
      [
        'type'     => 'type1',
        'severity' => RfcLogLevel::WARNING,
        'message'  => '3: This message contains substring and should not be logged.',
      ],
      [
        'type'     => 'type2',
        'severity' => RfcLogLevel::INFO,
        'message'  => '4: This message contains substring and should not be logged.',
      ],
    ], $messages);

    $this->assertEquals(1, $this->storage->countChannel('channel1'));
    $this->assertEquals(4, $this->storage->countChannel('channel2'));

    $this->storage->clearChannel('channel1');

    $this->assertEquals(0, $this->storage->countChannel('channel1'));
    $this->assertEquals(4, $this->storage->countChannel('channel2'));

  }

}
